﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountdownScript : MonoBehaviour
{
    void Awake()
    {
        CustomEventManager.onStartCountdown += StartAnim;
    }

    void StartAnim()
    {
        GetComponent<Animator>().enabled = true;
    }

    void OnDestroy()
    {
        CustomEventManager.onStartCountdown -= StartAnim;
    }
}
