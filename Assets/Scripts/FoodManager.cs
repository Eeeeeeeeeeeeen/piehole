﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FoodManager : MonoBehaviour
{
    public Transform[] locations = new Transform[4];
    public List<FoodObject> foodTypes;
    public List<FoodObject> activeFoods = new List<FoodObject>();

    public FoodObject initialFood;

    // Start is called before the first frame update
    void Awake()
    {
        activeFoods.Add(Instantiate(GetRandomFood(), locations[(int)Position.Eating].transform.position, Quaternion.identity));
        activeFoods.Add(Instantiate(GetRandomFood(), locations[(int)Position.Waiting].transform.position, Quaternion.identity));
        activeFoods.Add(Instantiate(GetRandomFood(), locations[(int)Position.Spawning].transform.position, Quaternion.identity));

        activeFoods[0].conveyorPosition = Position.Eating;
        activeFoods[1].conveyorPosition = Position.Waiting;
        activeFoods[2].conveyorPosition = Position.Spawning;
        
        CustomEventManager.onFoodComplete += MoveConveyor;
    }

    void Start()
    {
        CustomEventManager.onStartGame += Kickoff;
    }

    void Kickoff()
    {
        activeFoods[0].Play();
    }

    void MoveConveyor()
    {
        var foodForDeletion = activeFoods.Find(f => f.conveyorPosition == Position.End);
        if(foodForDeletion != null)
        {
            activeFoods.Remove(foodForDeletion);
            Destroy(foodForDeletion.gameObject);
        }

        foreach(var food in activeFoods)
        {
            var foodMovement = food.GetComponent<FoodMovement>();
            var newDestination = food.conveyorPosition + 1;
            foodMovement.SetDestination(locations[(int)newDestination].transform);
            food.conveyorPosition = newDestination;
        }
        
        var newFood = Instantiate(GetRandomFood(), locations[(int)Position.Spawning].transform.position, Quaternion.identity);
        newFood.conveyorPosition = Position.Spawning;
        activeFoods.Add(newFood);
    }

    FoodObject GetRandomFood()
    {
        int r = Random.Range(0, foodTypes.Count);
        return foodTypes[r];
    }

    void OnDestroy()
    {
        CustomEventManager.onFoodComplete -= MoveConveyor;
        CustomEventManager.onStartGame -= Kickoff;
    }
}
