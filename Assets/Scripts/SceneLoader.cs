﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : Singleton<SceneLoader>
{
    // guarantee this will be always a singleton only - can't use the constructor!
    protected SceneLoader() { }

    void Start()
    {
        LoadScene();
    }

    public void LoadScene()
    {
        LoadScene("Camera");
    }

    private void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }
}
