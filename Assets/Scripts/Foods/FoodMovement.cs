﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodMovement : MonoBehaviour
{
    private float speed = 5f;

    private Vector3 start;
    public Vector3 des;
    private float fraction = 1;
    private bool isMoving;

    void Start () {
        start = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    } 
    
    void Update() {
        if (fraction < 1) {
            fraction += Time.deltaTime * speed;
            transform.position = Vector3.Lerp(start, des, fraction);
        } else if(isMoving)
        {
            start = des;
            isMoving = false;
        }
    }

    public void SetDestination(Transform destination)
    {
        fraction = 0;
        des = destination.position;
        isMoving = true;
    }


}
