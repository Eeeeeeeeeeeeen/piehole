﻿public class Test : FoodObject
{
    public override void Awake()
    {
        _actions.Add(() => CustomEventManager.RaiseOnStartTyping("This"));
        _actions.Add(() => CustomEventManager.RaiseOnStartTyping("Is"));
        _actions.Add(() => CustomEventManager.RaiseOnStartTyping("A"));
        _actions.Add(() => CustomEventManager.RaiseOnStartTyping(name));

        base.Awake();
    }
}
