using UnityEngine;

public class Pie : FoodObject
{
    private string[] flavours = new string[5] {"Cherry", "Banoffee", "Pumpkin" , "Custard", "Rhubarb"};
    public override void Awake() {
        _actions.Add(() => CustomEventManager.RaiseOnStartMashing(10));
        _actions.Add(() => CustomEventManager.RaiseOnStartTyping($"{GetFlavour()} {name}"));
        
        base.Awake();
    }

    private string GetFlavour()
    {
        var r = Random.Range(0, flavours.Length);
        return flavours[r];
    }
}