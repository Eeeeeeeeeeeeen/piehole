public class Pizza : FoodObject
{
    public override void Awake()
    {
        _actions.Add(() => CustomEventManager.RaiseOnStartMashing(3));
        _actions.Add(() => CustomEventManager.RaiseOnStartTyping(name));

        base.Awake();
    }
}
