using System;
using System.Collections.Generic;
using UnityEngine;

public class FoodObject: MonoBehaviour, IFood
{
   // public Food food;
    public new string name;
    private int _actionCount = 0;
    private bool _isActive;

    protected List<Action> _actions = new List<Action>();

    public Position conveyorPosition;

    public virtual void Awake()
    {
        CustomEventManager.onStageCompleted += CheckFinished;

        if(GetComponent<FoodMovement>() == null)
        {
            FoodMovement foodMovement = gameObject.AddComponent(typeof(FoodMovement)) as FoodMovement;
        }
    }

    void Start()
    {
       // GetComponent<MeshFilter>().mesh = food.mesh;
    }

    public void Play()
    {
        if(conveyorPosition == Position.Eating)
        {
            _actions[_actionCount].Invoke();
            _actionCount++;
        }
    }

    protected void CheckFinished()
    {
        if(!(_actionCount == _actions.Count))
        {
            Play();
        } else 
        {
            CustomEventManager.RaiseOnFoodComplete();
            CustomEventManager.onStageCompleted -= CheckFinished;
        }
    }

    void OnDestroy()
    {
        CustomEventManager.onStageCompleted -= CheckFinished;
    }
}