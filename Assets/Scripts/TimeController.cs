﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class TimeController : MonoBehaviour
{
    public float _time = 10f;
    public TMP_Text _timerText;
    private bool isActive;
    void Awake()
    {
        _timerText = GetComponent<TMP_Text>();
    }

    // Start is called before the first frame update
    void Start()
    {
        CustomEventManager.onStartGame += StartTimer;
    }

    // Update is called once per frame
    void Update()
    {
        if(isActive)
        {
            _time -= Time.deltaTime;
            var currentTime = Mathf.Round(_time);
            _timerText.text = currentTime.ToString() + "s";

            if(currentTime == 0)
            {
                isActive = false;
                CustomEventManager.RaiseOnGameOver(0);
                PlayerPrefs.Save();
                SceneManager.LoadScene("Camera");
            }
        }
    }

    void StartTimer()
    {
        isActive = true;
        _timerText.enabled = true;
    }

    void DecreaseTime()
    {
        _time--;
    }

    void OnDestroy()
    {
        CustomEventManager.onStartGame -= StartTimer;
    }
}
