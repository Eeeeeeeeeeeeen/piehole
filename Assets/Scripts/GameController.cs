﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class GameController : MonoBehaviour
{
    private TMP_Text ScorePrompt;

    private int _score = 0;
    void Awake()
    {
        CustomEventManager.onFoodComplete += AddScore;
        ScorePrompt = GameObject.FindGameObjectWithTag("Score").GetComponent<TMP_Text>();
    }

    void AddScore()
    {
        _score ++;
        ScorePrompt.text = $"Score: {_score}";
        PlayerPrefs.SetInt("Score", _score);
    }

    void OnDestroy()
    {
        CustomEventManager.onFoodComplete -= AddScore;
    }
}
