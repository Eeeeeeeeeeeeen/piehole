
using UnityEngine;

public class ButtonMashController : MonoBehaviour
{
    private bool _enabled;
    private int _goal;
    private int _presses;

    public void Awake()
    {
        CustomEventManager.onStartMashing += StartMashing;
    }

    void StartMashing(int goal)
    {
        _enabled = true;
        _presses = 0;
        _goal = goal;
    }

    void Update()
    {
        if(_enabled)
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {
                _presses++;
            }

            if(_presses == _goal)
            {
                _enabled = false;
                CustomEventManager.RaiseDespawnGUI();
                CustomEventManager.RaiseOnStageCompleted();
            }
        }
    }

    void OnDestroy()
    {
        CustomEventManager.onStartMashing -= StartMashing;
    }
}
