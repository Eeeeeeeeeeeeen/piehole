using System;
using System.Collections;
using System.Linq;
using TMPro;
using UnityEngine;

public class TypingController : MonoBehaviour
{
    private bool _enabled;
    private int[] values;
    private bool[] keys;

    private bool _isChoking = false;

    void Start()
    {
        //Keycode for key 'A' and the other 25 letters of the alphabet
        values = Enumerable.Range(97, 26).ToArray();
        keys = new bool[values.Length];
        CustomEventManager.onStartTyping += StartTyping;
        CustomEventManager.onTypingCompleted += Disable;
        CustomEventManager.onStartChoking += FlipChoke;
        CustomEventManager.onStopChoking += FlipChoke;
    }

    // Update is called once per frame
    void Update()
    {
        if(_enabled)
        {
            var character = GetTypingInput();
            if(character != null && !_isChoking)
            {
                CustomEventManager.RaiseOnType(character);
            }
        }
    }

    string GetTypingInput()
    {
        //Check the values list for key then get the name of the key pressed
        string character = null;
        for(int i = 0; i < values.Length; i++) {
            keys[i] = Input.GetKeyDown((KeyCode)values[i]);
            if(keys[i])
                character = Enum.GetName(typeof(KeyCode), values[i]);
        }

        return character;
    }

    void StartTyping(string word)
    {
        _enabled = true;
        CustomEventManager.RaiseOnSpawnGUI(word);
    }

    void Disable()
    {
        _enabled = false;
        CustomEventManager.RaiseOnStageCompleted();
    }

    void FlipChoke()
    {
        _isChoking = !_isChoking;
    }

    void OnDestroy()
    {
        CustomEventManager.onStartTyping -= StartTyping;
        CustomEventManager.onTypingCompleted -= Disable;
        CustomEventManager.onStartChoking -= FlipChoke;
        CustomEventManager.onStopChoking -= FlipChoke;
    }
}
