﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class StartGame : MonoBehaviour
{
    public Animator _splashAnimator;
    public Animator _countdownAnimator;
    public TMPro.TMP_Text _finalScore;

    void Awake()
    {
        SceneLoaderAsync.Instance.LoadScene();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) && _splashAnimator != null)
        {
            _splashAnimator.SetBool("StartGame", true);
        }

        // if(Input.GetKeyDown(KeyCode.Alpha1))
        // {
        //    // SceneLoader.Instance.LoadScene();
        //     SceneManager.LoadScene("Camera");
        // }
        int score = PlayerPrefs.GetInt("Score");
        
        if(score != 0)
        {
            _finalScore.text = $"Your final score was\n <size=200%>{score}";
        }
    }
}
