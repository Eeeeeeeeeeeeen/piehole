using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PromptEventListener : MonoBehaviour
{
    private Animator _animator;

    void Awake()
    {
        _animator = GetComponent<Animator>();
        CustomEventManager.onStartTyping += TypingPrompt;
        CustomEventManager.onStartMashing += MashingPrompt;
    }
    
    void TypingPrompt(string character)
    {
        _animator.SetBool("PromptMash", false);
        _animator.SetBool("PromptType", true);
    }

    void MashingPrompt(int m)
    {
         _animator.SetBool("PromptType", false);
        _animator.SetBool("PromptMash", true);
    }

    void OnDestroy()
    {
        CustomEventManager.onStartTyping -= TypingPrompt;
        CustomEventManager.onStartMashing -= MashingPrompt;
    }
}
