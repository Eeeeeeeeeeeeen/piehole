using System;
using UnityEngine;

public class CustomEventManager : MonoBehaviour
{
    public delegate void UpdateGUIAction(string word);
    public static event UpdateGUIAction onUpdateGUI;
    
    public delegate void DespawnGUIAction();
    public static event DespawnGUIAction onDespawnGUI;

    public delegate void TypingAction(string character);
    public static event TypingAction onType;

    public delegate void StageCompletedAction();
    public static event StageCompletedAction onStageCompleted;

    public delegate void SpawnTypingGUIAction(string word);
    public static event SpawnTypingGUIAction onSpawnGUI;

    public delegate void FoodCompleteAction();
    public static event FoodCompleteAction onFoodComplete;

    //Start an action with a number to achieve
    public delegate void StartMashingAction(int goal);
    public static event StartMashingAction onStartMashing;

    public delegate void StartTypingAction(string word);
    public static event StartTypingAction onStartTyping;

    public delegate void StartGameAction();
    public static event StartGameAction onStartGame;

    public delegate void GameOverAction(int score);
    public static event GameOverAction onGameOver;

    public delegate void TypingCompletedAction();
    public static event TypingCompletedAction onTypingCompleted;

    public delegate void StartCountdownAction();
    public static event StartCountdownAction onStartCountdown;

    public delegate void StartChokingAction();
    public static event StartChokingAction onStartChoking;
    
    public delegate void StopChokingAction();
    public static event StopChokingAction onStopChoking;

    public static void RaiseOnType(string character)
    {
        if(onType != null)
        {
            onType(character);
        }
    }

    public static void RaiseOnStageCompleted()
    {
        if(onStageCompleted != null)
        {
            onStageCompleted();
        }
    }

    public static void RaiseOnSpawnGUI(string word)
    {
        if(onSpawnGUI != null)
        {
            onSpawnGUI(word);
        }
    }

    public static void RaiseOnFoodComplete()
    {
        if(onFoodComplete != null)
        {
            onFoodComplete();
        }
    }

    public static void RaiseOnStartMashing(int goal)
    {
        if(onStartMashing != null)
        {
            onStartMashing(goal);
        }
    }

    public static void RaiseOnStartTyping(string word)
    {
        if(onStartTyping != null)
        {
            onStartTyping(word);
        }
    }

    public static void RaiseOnTypingCompleted()
    {
        if(onTypingCompleted != null)
        {
            onTypingCompleted();
        }
    }

    public static void RaiseOnUpdateGUI(string word)
    {
        if(onUpdateGUI != null)
        {
            onUpdateGUI(word);
        }
    }

    public static void RaiseDespawnGUI()
    {
        if(onDespawnGUI != null)
        {
            onDespawnGUI();
        }
    }

    public static void RaiseStartGame()
    {
        if(onStartGame != null)
        {
            onStartGame();
        }
    }

    public static void RaiseOnGameOver(int score)
    {
        if(onGameOver != null)
        {
            onGameOver(score);
        }
    }

    public static void RaiseOnStartCountdown()
    {
        if(onStartCountdown != null)
        {
            onStartCountdown();
        }
    }

    public static void RaiseOnStartChoking()
    {
        if(onStartChoking != null)
        {
            onStartChoking();
        }
    }

    public static void RaiseOnStopChoking()
    {
        if(onStopChoking != null)
        {
            onStopChoking();
        }
    }
}
