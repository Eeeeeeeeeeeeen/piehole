﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class WordManager : MonoBehaviour
{
    public GameObject textMeshPrefab;

    public GameObject spacePrefab;
    
    private GameObject GUI;

    private TMP_Text _textComponent;

    private GameObject _spaceObj;

    private int currentCharacter = 0;

    void Awake()
    {
        
        CustomEventManager.onSpawnGUI += SpawnNewWord;
        CustomEventManager.onUpdateGUI += UpdateWord;
        CustomEventManager.onDespawnGUI += DespawnWord;
        CustomEventManager.onGameOver += DespawnWord;
       // CustomEventManager.onGameOver += Des;
        CustomEventManager.onType += CheckWord;
        CustomEventManager.onStartMashing += SpawnSpace;
    }

    void Start()
    {
        GUI = GameObject.FindGameObjectWithTag("GUI");

    }

    void SpawnNewWord(string word)
    {
        if(GUI == null)
        {
         GUI = GameObject.FindGameObjectWithTag("GUI");   
        }
        var textMesh = Instantiate(textMeshPrefab, GUI.transform, false);
        _textComponent = textMesh.GetComponent<TMP_Text>();

        UpdateWord(word);
    }

    void UpdateWord(string word)
    {
        _textComponent.text = word;
    }

    void DespawnWord()
    {
        if(_textComponent != null)
        {
            Destroy(_textComponent.gameObject);
        }

        if(_spaceObj != null)
        {
            Destroy(_spaceObj.gameObject);
        }
    }

    void DespawnWord(int score)
    {
        if(_textComponent != null)
        {
            Destroy(_textComponent.gameObject);
        }

        if(_spaceObj != null)
        {
            Destroy(_spaceObj.gameObject);
        }
    }

    void SpawnSpace(int a)
    {
                if(GUI == null)
        {
         GUI = GameObject.FindGameObjectWithTag("GUI");   
        }
        var space = Instantiate(spacePrefab, GUI.transform, false);
        _spaceObj = space;
    }

    void CheckWord(string character)
    {
        if (_textComponent.text[currentCharacter].ToString().ToUpper() == character)
        {
            TMP_TextInfo textInfo = _textComponent.textInfo;

            Color32[] newVertexColors;
            Color32 c0 = _textComponent.color;

            int characterCount = textInfo.characterCount;

            // Get the index of the material used by the current character.
            int materialIndex = textInfo.characterInfo[currentCharacter].materialReferenceIndex;

            // Get the vertex colors of the mesh used by this text element (character or sprite).
            newVertexColors = textInfo.meshInfo[materialIndex].colors32;

            // Get the index of the first vertex used by this text element.
            int vertexIndex = textInfo.characterInfo[currentCharacter].vertexIndex;

            // Only change the vertex color if the text element is visible.
            if (textInfo.characterInfo[currentCharacter].isVisible)
            {
                c0 = new Color32((byte)Random.Range(0, 255), (byte)Random.Range(0, 255), (byte)Random.Range(0, 255), 255);

                newVertexColors[vertexIndex + 0] = c0;
                newVertexColors[vertexIndex + 1] = c0;
                newVertexColors[vertexIndex + 2] = c0;
                newVertexColors[vertexIndex + 3] = c0;

                // New function which pushes (all) updated vertex data to the appropriate meshes when using either the Mesh Renderer or CanvasRenderer.
                _textComponent.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);

                // This last process could be done to only update the vertex data that has changed as opposed to all of the vertex data but it would require extra steps and knowing what type of renderer is used.
                // These extra steps would be a performance optimization but it is unlikely that such optimization will be necessary.
            }

            if(currentCharacter == characterCount - 1)
            {
                DespawnWord();
                CustomEventManager.RaiseOnTypingCompleted();
            }
            
            currentCharacter = (currentCharacter + 1) % characterCount;

            if(_textComponent.text[currentCharacter].ToString() == " ")
                currentCharacter = (currentCharacter + 1) % characterCount;
        } else{
                CustomEventManager.RaiseOnStartChoking();
            }
    }

    void OnDestroy()
    {
        CustomEventManager.onSpawnGUI -= SpawnNewWord;
        CustomEventManager.onUpdateGUI -= UpdateWord;
        CustomEventManager.onDespawnGUI -= DespawnWord;
        CustomEventManager.onGameOver -= DespawnWord;
        CustomEventManager.onType -= CheckWord;
        CustomEventManager.onStartMashing -= SpawnSpace;
    }
}
