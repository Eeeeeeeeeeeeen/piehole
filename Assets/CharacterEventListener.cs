﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterEventListener : MonoBehaviour
{
    private Animator _animator;

    void Awake()
    {
        _animator = GetComponent<Animator>();
        CustomEventManager.onType += StartEating;
        CustomEventManager.onStartChoking += StartChoking;
    }
    
    void StartEating(string character)
    {
        _animator.SetTrigger("IsEating");
    }

    void StartChoking()
    {
        _animator.SetTrigger("IsChoking");
    }
    
    void OnDestroy()
    {
        CustomEventManager.onType -= StartEating;
        CustomEventManager.onStartChoking -= StartChoking;
    }
}
